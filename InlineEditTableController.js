({
    initRecords: function(component, event, helper) {
        var action = component.get("c.fetchRecords");
        component.set('v.showSpinner',true);
        component.set('v.recordId',component.get("v.recordId"));
        action.setParams({
            recordId : component.get("v.recordId"),
            offset : 0
        });
        try {
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    
                    var storeResponse = response.getReturnValue();
                    
                    // set AccountList list with return value from server.
                    component.set("v.AccountList", storeResponse.dueDiliganceWrapperList);
        			component.set("v.fieldLabelList", storeResponse.mapOfFields);
                    component.set("v.backUpRecords",storeResponse.dueDiliganceWrapperList);
                    component.set("v.recordCount",storeResponse.recordCount);
                    component.set("v.showTable",true);
                    var action = component.get('c.disablePreviousNext');
                    $A.enqueueAction(action);
                }
                component.set('v.showSpinner',false);
            });
            $A.enqueueAction(action);
        } catch (error) {
            component.set('v.showSpinner',false);
            console.log(error);
        }
    },
    handleKeyUp: function (component, event,helper) {
        var isEnterKey = event.keyCode === 13;
        if (isEnterKey) {
            helper.fetchRecord(component, event, helper,0);
            component.set('v.offset',0);
            // var action = component.get('c.disablePreviousNext');
            // $A.enqueueAction(action);
        }
    },



    handleSort : function(component,event,helper){
        try {
            component.set('v.showSpinner',true);
            var sortBy = event.target.name;
            var colNo = event.target.dataset.col;
            var sortDir = event.target.dataset.sortdir;
            setTimeout(function(){
                helper.sortData(component,sortBy,sortDir);
                sortDir = sortDir == 'dsc' ? 'asc' : 'dsc';
                var fieldLabel = component.get('v.fieldLabelList');
                fieldLabel[colNo].sortDirection = sortDir;
                component.set('v.fieldLabelList',fieldLabel);
                component.set('v.showSpinner',false);   
                component.set('v.sortedField',fieldLabel[colNo]); 
            },700);
        } catch(ex){
            component.set('v.showSpinner',false);
        }
    },

    Save: function(component, event, helper) {
        component.set('v.showSpinner',true);
        var action = component.get("c.saveRecord");
        var updateList = [];
        var recordList = [...component.get("v.AccountList")];
        recordList.forEach(element => {
            if (element.objectRecord.isChanged) {
                // component.get('v.fieldLabelList').forEach(ele =>{
                    updateList.push(element.objectRecord);
                // })
            }
        });
        action.setParams({
            'lstSObject': JSON.stringify(updateList)
        });
        try {
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var response = response.getReturnValue();
                    let message = 'Updated successfully';
                    let title = 'Success';
                    if (response == undefined || response == '') {
                        let records = component.get("v.AccountList")
                        records.forEach(element=>{
                            element.objectRecord.isChanged = false;
                            element.mapOfSingleRecordData.forEach(ele=>{
                                ele.isChanged = false;
                            })
                        });
                        component.set('v.AccountList',records);
                        component.set("v.showSaveCancelBtn",false);
                    } else {
                        message = response;
                        title = 'Error';
                    }
                    helper.showToast(component,message,title);
                }
                component.set('v.offset',0);
                var action = component.get('c.disablePreviousNext');
                $A.enqueueAction(action);
                component.set('v.showSpinner',false);
            });
            $A.enqueueAction(action);
        } catch (error) {
            console.log(error);
        }
        
        // } 
    },
    
    cancel : function(component,event,helper){
        helper.fetchRecord(component, event, helper, component.get('v.offset'))
        component.set("v.showSaveCancelBtn",false);
        // var action = component.get('c.disablePreviousNext');
        // $A.enqueueAction(action);
        // $A.get('e.force:refreshView').fire(); 
    },

    nextPage : function(component, event, helper) {
        var attribute = event.getSource().get('v.name');
        var offset = component.get('v.offset');
        if (attribute=='previous') {
            offset = offset-20;
        } else {
            offset += 20; 
        }
        component.set('v.offset',offset);
        helper.fetchRecord(component, event, helper,offset);
        // var action = component.get('c.disablePreviousNext');
        // $A.enqueueAction(action);
        // if (offset > 0) {
        //     component.set('v.disablePrevious',false);
        // } else {
        //     component.set('v.disablePrevious',true);
        // }
        // if (offset > 80) {
        //     component.set('v.disableNext',true);
        // } else {
        //     component.set('v.disableNext',false);
        // }
    },

    disablePreviousNext : function(component, event, helper) {
        var offset = component.get('v.offset');
        var recordCount = component.get("v.recordCount");
        if (recordCount < 20) {
            component.set('v.disableNext',true);
            component.set('v.disablePrevious',true);
        } else {
            if (offset > 0) {
                component.set('v.disablePrevious',false);
            } else {
                component.set('v.disablePrevious',true);
            }
            if (offset > 1800) {
                component.set('v.disableNext',true);
            } else {
                component.set('v.disableNext',false);
            }
        }   
    }
})