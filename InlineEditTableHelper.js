({
    
    sortData : function(component,fieldName,sortDirection){
        var data = component.get("v.AccountList");
        //function to return the value stored in the field
        var key = function(a) { return a[fieldName]; }
        var reverse = sortDirection == 'asc' ? 1: -1;
        
        // to handel number/currency type fields 
        if(fieldName == 'NumberOfEmployees'){ 
            data.sort(function(a,b){
                var a = key(a.objectRecord) ? key(a.objectRecord) : '';
                var b = key(b.objectRecord) ? key(b.objectRecord) : '';
                return reverse * ((a>b) - (b>a));
            }); 
        }
        else{// to handel text type fields 
            data.sort(function(a,b){ 
                var a = key(a.objectRecord) ? key(a.objectRecord).toLowerCase() : '';//To handle null values , uppercase records during sorting
                var b = key(b.objectRecord) ? key(b.objectRecord).toLowerCase() : '';
                return reverse * ((a>b) - (b>a));
            });    
        }
        //set sorted data to accountData attribute
        component.set("v.AccountList",data);
    },
    
    showToast : function(component, message,title) {
        try {
            var toastEvent = $A.get("e.force:showToast");
            if (title == 'Error') {
                toastEvent.setParams({
                    "title": title,
                    "message": message,
                    'type' : 'error'	
                });
            } else {
                toastEvent.setParams({
                    "title": title,
                    "message": message,
                    'type' : 'success'	
                });
            }
            toastEvent.fire();
        } catch (error) {
            console.log(error);
        }
        
    },

    fetchRecord : function(component, event, helper, offset) {
        var sortBy = component.get('v.sortedField'); 
        var searchKey = component.find('enter-search').get('v.value');
        var picklistValue = component.find("responsibleParty").get("v.value");
        component.set("v.selectedValue",picklistValue);
        var statusPicklistValue = component.find("status").get("v.value");
        component.set("v.statusSelectedValue",statusPicklistValue);
        var folderPicklistValue = component.find("folder").get("v.value");
        component.set("v.folderSelectedValue",folderPicklistValue);
        component.set('v.showSpinner',true);
        let action = component.get('c.getSearchedRecord');
        let sortDir = 'asc' ;
        let sortField = '';
        if(sortBy != undefined) {
            sortDir = sortBy.sortDirection == 'asc' ? 'asc' : 'desc' ;
            sortField = sortBy.apiName ;
        } 
        let obj = {
            responsibleParty : picklistValue,
            status : statusPicklistValue,
            category : folderPicklistValue,
            searchKey : searchKey,
            offset : offset,
            sortBy : sortField,
            sortDir : sortDir
        };
        action.setParams({
            loanId : component.get('v.recordId'),
            jsonFilter : JSON.stringify(obj)
        });
        action.setCallback(this, function(response){
            try {
                var state = response.getState();
                if (state == 'SUCCESS') {
                    console.log(response.getReturnValue());
                    let storeResponse = response.getReturnValue();
                    component.set("v.AccountList", storeResponse.dueDiliganceWrapperList);
                    component.set("v.backUpRecords",storeResponse.dueDiliganceWrapperList);
                    component.set("v.recordCount",storeResponse.recordCount);
                    var recordCount = storeResponse.recordCount;
                    if (recordCount < 20) {
                        component.set('v.disableNext',true);
                        component.set('v.disablePrevious',true);
                    } else {
                        if (offset > 0) {
                            component.set('v.disablePrevious',false);
                        } else {
                            component.set('v.disablePrevious',true);
                        }
                        if (offset > 1800) {
                            component.set('v.disableNext',true);
                        } else {
                            component.set('v.disableNext',false);
                        }
                    } 
                }
                component.set('v.showSpinner',false);
            } catch(ex) {
                component.set('v.showSpinner',false);
            }
            
        });
        $A.enqueueAction(action);
    }
})