({
	
    firstPromise : function(component, event, helper,col, row) {
        return new Promise(function(resolve, reject) {
          // do something
          try {
                var record = component.get('v.items');
                record[row].mapOfSingleRecordData[col-1]['editField'] = true;
                component.set('v.items',record);
                component.set('v.showSpinner',false);
                setTimeout(function(){
                    component.find("inputFocus").focus();
                },100);
                resolve();
          }
          catch(ex) {
            reject("Rejected");
          }
        });
    },

    // closeInput : function(component, event, helper,col, row) {
    //     return new Promise(function(resolve, reject) {
    //       // do something
    //       try {
    //             var record = component.get('v.items');
    //             record[row].mapOfSingleRecordData[col-1]['editField'] = false;
    //             component.set('v.items',record);
    //             component.set('v.showSpinner',false);
    //             resolve();
    //       }
    //       catch(ex) {
    //         reject("Rejected");
    //       }
    //     });
    // }
})