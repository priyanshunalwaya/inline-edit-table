({
    
    doInit: function(component, event, helper) {
        var url = window.location.origin;
        component.set('v.url',url);
        // var tempFields = component.get('v.fieldLabelList');
        // tempFields.forEach(element=>{
        //     if (element.apiName != 'S.No' && element.apiName != 'Id') {
        //         fields.push(element.apiName);
        //     }
        // });
        // component.set('v.fields',fields);
    },
    inlineEditText : function(component,event,helper){   
        // show the name edit field popup 
        try {
            component.set('v.showSpinner',true);
            var row = event.currentTarget.dataset.row;
            var col = event.currentTarget.dataset.col;
            var _component = component;
            
           helper.firstPromise(component, event, helper,col,row)
           .then(res=>{
               component.set('v.showSpinner',false);
           })
           .catch(error=>{
               console.log(error);
           })
           .finally(final=>{
            component.set('v.showSpinner',false);
           });
            
            // setTimeout(function(){
            //     try {
            //         var record = _component.get('v.items');
            //     record[row].mapOfSingleRecordData[col-1]['editField'] = true;
            //     console.log(record,'record');
            //     _component.set('v.items',record);
            //     _component.set('v.showSpinner',false);
            //     setTimeout(function(){
            //         _component.find("inputFocus").focus();
            //     },100);
            //     _component.set('v.showSpinner',false);
            //     }catch(ex) {
            //         console.log(ex);
            //     }
                
                
            // },500);
            
        } catch(error) {
            console.log(error);
            component.set('v.showSpinner',false);
        }
    },
    
    inlineEditPicklist : function(component, event, helper){   
        // show the rating edit field popup 
        try {
            component.set('v.showSpinner',true);
            
            var col = event.currentTarget.dataset.col;
            var row = event.currentTarget.dataset.row;
            // firstPromise(col, row)
            // setTimeout(function(){
                component.set('v.editedCol',col);
                component.set('v.editedRow',row);
                var record = component.get('v.items');
                let fieldLabelList = component.get('v.fieldLabelList');
                // component.set('v.dilligenceId',record[row].objectRecord.Id);
                component.set('v.selectedField',fieldLabelList[col].label);
                // helper.fetchPickListVal(component,record[row].mapOfSingleRecordData[col-1].key,'picklistValues');
                component.set('v.showSpinner',false);
                record[row].mapOfSingleRecordData[col-1]['editField'] = true;
                component.set('v.items',record);
                setTimeout(function(){
                    component.find("picklist").focus();
                },100);
            // },500);
            
        }
        catch(ex) {
            component.set('v.showSpinner',false);
            console.log(ex);
        }
    },
    
     onNameChange : function(component,event,helper){
        var col = event.getSource().get("v.name");
        var value = event.getSource().get("v.value");
        if (typeof value){
            value = value.trim();
        }
        try{
            let row = parseInt(col.split(',')[1]);
            col = parseInt(col.split(',')[0]);
            var record = component.get('v.items');
            var editedRecord = record[row];
            if (value != editedRecord.objectRecord[editedRecord.mapOfSingleRecordData[col-1].key]) {
                editedRecord.objectRecord[editedRecord.mapOfSingleRecordData[col-1].key] = value;
                editedRecord.mapOfSingleRecordData[col-1]['isChanged'] = true;
                editedRecord.objectRecord['isChanged'] = true;
                editedRecord.mapOfSingleRecordData[col-1]['value'] = value;
                record[row] = editedRecord;
                component.set("v.showSaveCancelBtn",true);
            }  
            editedRecord.mapOfSingleRecordData[col-1]['editField'] = false; 
            component.set('v.items',record);            
            
            
        } catch(ex) {
            // component.set('v.showSpinner',false);
            console.log(ex);
        }
        // }
    },
 
    onPicklistChange : function(component,event,helper){ 
        try {
            // component.set('v.showSpinner',true);
            var col = event.getSource().get("v.name");
            var value = event.getSource().get("v.value");
            component.set('v.recordPopUp',false);
            setTimeout(function(){
                let row = parseInt(col.split(',')[1]);
                col = parseInt(col.split(',')[0]);
                var record = component.get('v.items');
                var editedRecord = record[row];
                if (value != editedRecord.objectRecord[editedRecord.mapOfSingleRecordData[col-1].key] ) {
                    editedRecord.objectRecord[editedRecord.mapOfSingleRecordData[col-1].key] = value;
                    editedRecord.objectRecord['isChanged'] = true;
                    editedRecord.mapOfSingleRecordData[col-1]['isChanged'] = true;
                    editedRecord.mapOfSingleRecordData[col-1]['value'] = value;
                    record[row] = editedRecord;
                    component.set("v.showSaveCancelBtn",true);
                }
                editedRecord.mapOfSingleRecordData[col-1]['editField'] = false;
                component.set('v.items',record);
                component.set('v.showSpinner',false);
            },500);
        } catch (error) {
            console.log(error);
            // component.set('v.showSpinner',false);
        }
        
    },     
    
    closeNameBox : function (component, event, helper) {
        component.set('v.showSpinner',true);
        
        try {
            var col = event.getSource().get("v.name");
        // setTimeout(function(){
            let row = parseInt(col.split(',')[1]);
            col = parseInt(col.split(',')[0]);
            // helper.closeInput(component, event, helper,col,row);
            var record = component.get('v.items');
            record[row].mapOfSingleRecordData[col-1]['editField'] = false;
            component.set('v.items',record);
            // component.set('v.recordPopUp',false);
            component.set('v.showSpinner',false);
        // },500);
        }catch(ex) {
            component.set('v.showSpinner',false);
            console.log(ex);
        }
    }, 
    
    
    // closeRatingBox : function (component, event, helper) {
    //    // on focus out, close the input section by setting the 'ratingEditMode' att. as false
    //     component.set("v.statusEditMode", false); 
    //     // component.set('v.typeEditMode',false);
    // },
    
    
})